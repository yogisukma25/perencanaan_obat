<!doctype html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/login/style.php');?>
</head>

<body>
    <div class="position-relative">
        <div class="authentication-wrapper authentication-basic container-p-y">
            <div class="authentication-inner py-4">
                <!-- Login -->
                <div class="card p-2">
                    <div class="card-body mt-2">
                        <h4 class="mb-6 text-center">Login</h4>
                        <p class="mb-4 text-center">Silahkan login untuk mengakses website</p>

                        <!-- Form Login -->
                        <form method="POST" action="<?=base_url('login/process')?>">
                            <div class="form-floating form-floating-outline mb-3">
                                <input type="email" class="form-control <?=isset($error) ? 'is-invalid' : ''?>"
                                    id="email" name="email" placeholder="Masukkan email" value="<?=set_value('email')?>"
                                    autofocus />
                                <label for="email">Email</label>
                                <?php if (isset($error)): ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?=$error?></strong>
                                </span>
                                <?php endif;?>
                            </div>
                            <div class="mb-3">
                                <div class="form-password-toggle">
                                    <div class="input-group input-group-merge">
                                        <div class="form-floating form-floating-outline">
                                            <input type="password"
                                                class="form-control <?=isset($error) ? 'is-invalid' : ''?>"
                                                id="password" name="password"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="password" />
                                            <label for="password">Password</label>
                                            <?php if (isset($error)): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <?=$error?>
                                            </span>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button class="btn btn-primary d-grid w-100" type="submit">Login</button>
                            </div>
                        </form>
                        <!-- End Form Login -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('includes/login/script.php');?>
</body>

</html>