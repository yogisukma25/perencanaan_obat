<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="<?php echo base_url('../../../assets/template/vendor/libs/jquery/jquery.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/libs/popper/popper.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/js/bootstrap.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/libs/node-waves/node-waves.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/js/menu.js') ?>"></script>

<!-- endbuild -->

<!-- Toastr CSS and JS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<!-- SweetAlert CSS and JS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.0.1/dist/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.0.1/dist/sweetalert2.all.min.js"></script>


<!-- Vendors JS -->
<script src="<?php echo base_url('../../../assets/template/vendor/libs/apex-charts/apexcharts.js') ?>"></script>

<!-- Main JS -->
<script src="<?php echo base_url('../../../assets/template/js/main.js') ?>"></script>

<!-- Page JS -->
<script src="<?php echo base_url('../../../assets/template/js/dashboards-analytics.js') ?>"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js'"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url('../../../assets/template/js/datatables-demo.js') ?>"></script>
<!-- Page level plugins -->
<script src="<?php echo base_url('../../../assets/template/vendor/libs/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('../../../assets/template/vendor/libs/datatables/dataTables.bootstrap4.min.js') ?>"></script>

<!--  tanggal/Flatpickr JS -->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
