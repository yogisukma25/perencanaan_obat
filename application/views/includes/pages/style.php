<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="<?php echo base_url('../../../../../assets/favicon.icon') ?>" />

<!-- Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300..900;1,300..900&display=swap"
    rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/fonts/materialdesignicons.css') ?>" />

<!-- Menu waves for no-customizer fix -->
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/libs/node-waves/node-waves.css') ?>" />

<!-- Core CSS -->
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/css/core.css" class="template-customizer-core-css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/css/theme-default.css') ?>"
    class="template-customizer-theme-css" />
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/css/demo.css') ?>" />

<!-- Vendors CSS -->
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/libs/apex-charts/apex-charts.css') ?>" />

<!-- Page CSS -->

<!-- Helpers -->
<script src="<?php echo base_url('../../../../../assets/template/vendor/js/helpers.js') ?>"></script>
<!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
<!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
<script src="<?php echo base_url('../../../../../assets/template/js/config.js ') ?>"></script>

<!-- Custom styles for this page -->
<link href="<?php echo base_url('../../../../../assets/template/vendor/libs/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">

<!-- tanggal/Add Flatpickr CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<!-- Select 2 -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

<link rel="stylesheet" href="<?php echo base_url('../../../../../assets/template/vendor/css/pages/page-auth.css') ?>" />
