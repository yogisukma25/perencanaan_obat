<?php
$current_url = current_url();

function is_active($route, $current_url)
{
    return strpos($current_url, site_url($route)) !== false ? 'active' : '';
}
?>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <img src="<?=base_url('../../../../../assets/template/img/logo-dinas-kesehatan.jpeg')?>" alt="Logo"
        class="w-50 p-3">
    <div class="menu-inner-shadow"></div>
    <ul class="py-1 menu-inner">
        <!-- Dashboard -->
        <li class="menu-item <?=is_active('dashboard', $current_url)?>">
            <a href="<?=site_url('dashboard')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-home-outline"></i>
                <div data-i18n="dashboard" style="font-weight: 600">Dashboard</div>
            </a>
        </li>
        <!-- Puskesmas -->
        <li class="menu-item <?=is_active('puskesmas', $current_url)?>">
            <a href="<?=site_url('puskesmas')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-hospital-building"></i>
                <div data-i18n="puskesmas" style="font-weight: 600">Puskesmas</div>
            </a>
        </li>
        <!-- Jenis Obat -->
        <!-- <li class="menu-item <?=is_active('jenis_obat', $current_url)?>">
            <a href="<?=site_url('jenis_obat')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-microscope"></i>
                <div data-i18n="jenis_obat" style="font-weight: 600">Jenis Obat</div>
            </a>
        </li> -->
        <!-- Obat -->
        <li class="menu-item <?=is_active('obat', $current_url)?>">
            <a href="<?=site_url('obat')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-pill"></i>
                <div data-i18n="obat" style="font-weight: 600">Obat</div>
            </a>
        </li>
        <!-- Perencanaan -->
        <li class="menu-item <?=is_active('perencanaan', $current_url)?>">
            <a href="<?=site_url('perencanaan')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-clipboard-text-outline"></i>
                <div data-i18n="perencanaan" style="font-weight: 600">Perencanaan</div>
            </a>
        </li>
        <!-- Pengajuan -->
        <li class="menu-item <?=is_active('pengajuan', $current_url)?>">
            <a href="<?=site_url('pengajuan')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-clipboard-check-outline"></i>
                <div data-i18n="pengajuan" style="font-weight: 600">Pengajuan</div>
            </a>
        </li>
        <!-- Role -->
        <!-- <li class="menu-item <?=is_active('role', $current_url)?>">
            <a href="<?=site_url('role')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-account-lock-outline"></i>
                <div data-i18n="role" style="font-weight: 600">Role</div>
            </a>
        </li> -->
        <!-- User -->
        <li class="menu-item <?=is_active('user', $current_url)?>">
            <a href="<?=site_url('user')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-account-multiple-outline"></i>
                <div data-i18n="user" style="font-weight: 600">User</div>
            </a>
        </li>
        <!-- Profile -->
        <li class="menu-item <?=is_active('profile', $current_url)?>">
            <a href="<?=site_url('profile')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-account-outline"></i>
                <div data-i18n="profile" style="font-weight: 600">Profile</div>
            </a>
        </li>
        <!-- Logout -->
        <li class="menu-item <?=is_active('login/logout', $current_url)?>">
            <a href="<?=site_url('login/logout')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-arrow-right-bold-circle-outline"></i>
                <div data-i18n="logout" style="font-weight: 600">Logout</div>
            </a>
        </li>
    </ul>
</aside>