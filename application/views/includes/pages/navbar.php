<nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar">
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="px-0 nav-item nav-link me-xl-4" href="javascript:void(0)">
            <i class="mdi mdi-menu mdi-24px"></i>
        </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <ul class="flex-row navbar-nav align-items-center ms-auto">
            <!-- Place this tag where you want the button to render. -->
            <!-- User -->
            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                <a class="p-0 nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                        <img src="<?=base_url('../../../../../assets/template/img/avatars/1.png')?>" alt
                            class="h-auto w-px-40 rounded-circle" />
                    </div>
                </a>
                <ul class="py-2 mt-3 dropdown-menu dropdown-menu-end">
                    <li>
                        <a class="pb-2 mb-1 dropdown-item" href="#">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 me-2 pe-1">
                                    <div class="avatar avatar-online">
                                        <img src="<?=base_url('../../../../../assets/template/img/avatars/1.png')?>" alt
                                            class="h-auto w-px-40 rounded-circle" />
                                    </div>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="mb-0">
                                        <!-- {{ Auth::user()->name }} -->
                                    </h6>
                                    <small class="text-muted">
                                        <!-- @if (implode(', ', Auth::user()->getRoleNames()->toArray()) == 'admin')
                                        Admin
                                        @endif
                                        @if (implode(', ', Auth::user()->getRoleNames()->toArray()) == 'teknisi')
                                        Teknisi
                                        @endif
                                        @if (implode(', ', Auth::user()->getRoleNames()->toArray()) == 'loket')
                                        Loket
                                        @endif
                                        @if (implode(', ', Auth::user()->getRoleNames()->toArray()) == 'Pelanggan')
                                        Pelanggan
                                        @endif -->
                                    </small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="my-1 dropdown-divider"></div>
                    </li>
                </ul>
            </li>
            <!--/ User -->
        </ul>
    </div>
</nav>