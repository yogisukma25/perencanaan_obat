<?php 
$current_url = current_url();

function is_active($route, $current_url) {
    return strpos($current_url, site_url($route)) !== false ? 'active' : '';
}
?>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <img src="<?=base_url('../../../../../assets/template/img/logo-dinas-kesehatan.jpeg')?>" alt="Logo"
        class="w-50 p-3">
    <div class="menu-inner-shadow"></div>
    <ul class="py-1 menu-inner">
        <!-- Dashboard -->
        <li class="menu-item <?= is_active('dashboard', $current_url) ?>">
            <a href="<?=site_url('dashboard')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-home-outline"></i>
                <div data-i18n="dashboard" style="font-weight: 600">Dashboard</div>
            </a>
        </li>
        <!-- Perencanaan -->
        <li class="menu-item <?= is_active('perencanaanverifikator', $current_url) ?>">
            <a href="<?=site_url('perencanaanverifikator')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-clipboard-text-outline"></i>
                <div data-i18n="perencanaan" style="font-weight: 600">Perencanaan</div>
            </a>
        </li>
        <!-- Pengajuan -->
        <li class="menu-item <?= is_active('pengajuanverifikator', $current_url) ?>">
            <a href="<?=site_url('pengajuanverifikator')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-clipboard-check-outline"></i>
                <div data-i18n="pengajuan" style="font-weight: 600">Pengajuan</div>
            </a>
        </li>
        <!-- Logout -->
        <li class="menu-item <?= is_active('login/logout', $current_url) ?>">
            <a href="<?=site_url('login/logout')?>" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-arrow-right-bold-circle-outline"></i>
                <div data-i18n="logout" style="font-weight: 600">Logout</div>
            </a>
        </li>
    </ul>
</aside>