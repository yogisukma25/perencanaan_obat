<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/admin/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Edit Profil Pengguna</h4>
                                    </div>
                                    <div class="card-body">
                                        <?=form_open('profile/edit')?>
                                        <div class="form-group mb-3">
                                            <label for="nama">Nama Lengkap:</label>
                                            <input type="text" class="form-control" id="nama_lengkap"
                                                name="nama_lengkap"
                                                value="<?=set_value('nama_lengkap', $user->nama_lengkap)?>" required>
                                            <?=form_error('nama_lengkap', '<small class="text-danger">', '</small>')?>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="email">Email:</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="<?=set_value('email', $user->email)?>" required>
                                            <?=form_error('email', '<small class="text-danger">', '</small>')?>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="password">Password Baru:</label>
                                            <input type="password" class="form-control" id="password" name="password"
                                                value="<?=set_value('password')?>">
                                            <?=form_error('password', '<small class="text-danger">', '</small>')?>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="confirm_password">Konfirmasi Password:</label>
                                            <input type="password" class="form-control" id="confirm_password"
                                                name="confirm_password" value="<?=set_value('confirm_password')?>">
                                            <?=form_error('confirm_password', '<small class="text-danger">', '</small>')?>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        <?=form_close()?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>


    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    </script>

</body>

</html>