<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/admin/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Tambah Obat</h4>
                                        <a href="<?=site_url('obat')?>" class="btn btn-secondary btn-sm">Kembali</a>
                                    </div>
                                    <div class="card-body">
                                        <?php echo validation_errors(); ?>
                                        <?php echo form_open('obat/create'); ?>
                                        <div class="form-group mb-3">
                                            <label for="nama_obat">Nama Obat</label>
                                            <input type="text" class="form-control" id="nama_obat" name="nama_obat"
                                                value="<?=set_value('nama_obat')?>">
                                        </div>
                                        <!-- <div class="form-group mb-3">
                                            <label for="jenis_id">Jenis Obat</label>
                                            <select class="form-control" id="jenis_id" name="jenis_id">
                                                <?php foreach ($jenis_obat as $jenis): ?>
                                                <option value="<?=$jenis->id?>"><?=$jenis->nama_jenis?>
                                                </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div> -->
                                        <div class="form-group mb-3">
                                            <label for="harga">Harga</label>
                                            <input type="text" class="form-control" id="harga" name="harga"
                                                value="<?=set_value('harga')?>">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <a href="<?=site_url('obat')?>" class="btn btn-secondary">Batal</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>


    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    </script>

</body>

</html>