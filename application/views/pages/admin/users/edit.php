<!DOCTYPE html>
<html lang="<?= str_replace('_', '-', $this->config->item('language')) ?>" class="light-style layout-menu-fixed"
    dir="ltr" data-theme="theme-default" data-assets-path="<?= base_url('assets/template/') ?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php'); ?>
    <?php $this->load->view('includes/pages/style.php'); ?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/admin/sidebar.php'); ?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php'); ?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Edit User</h4>
                                        <a href="<?= site_url('user') ?>" class="btn btn-secondary btn-sm">Kembali</a>
                                    </div>
                                    <div class="card-body">
                                        <?php echo validation_errors(); ?>
                                        <?php echo form_open('user/edit/' . $user->id); ?>
                                        <div class="form-group mb-3">
                                            <label for="username">Username</label>
                                            <input type="text" class="form-control" id="username" name="username"
                                                value="<?= set_value('username', $user->username) ?>" required>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="name">Nama User</label>
                                            <input type="text" class="form-control" id="name" name="nama_lengkap"
                                                value="<?= set_value('nama_lengkap', $user->nama_lengkap) ?>" required>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="<?= set_value('email', $user->email) ?>" required>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="role_id">Role</label>
                                            <select class="form-control" id="role_id" name="role_id" required>
                                                <?php foreach ($roles as $role): ?>
                                                <option value="<?= $role->id ?>"
                                                    <?= ($role->id == $user->role_id) ? 'selected' : '' ?>>
                                                    <?= $role->role_name ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="puskesmas_id">Puskesmas</label>
                                            <select class="form-control" id="puskesmas_id" name="puskesmas_id">
                                                <option value=""> Bukan Puskesmas</option>
                                                <?php foreach ($puskesmas as $row): ?>
                                                <option value="<?= $row->id ?>"
                                                    <?= ($row->id == $user->puskesmas_id) ? 'selected' : '' ?>>
                                                    <?= $row->nama_puskesmas ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="password">Password (Kosongkan jika tidak diubah)</label>
                                            <input type="password" class="form-control" id="password" name="password"
                                                placeholder="Masukkan password jika ingin diubah">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="confirm_password">Konfirmasi Password</label>
                                            <input type="password" class="form-control" id="confirm_password"
                                                name="confirm_password" placeholder="Konfirmasi password jika diubah">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        <a href="<?= site_url('user') ?>" class="btn btn-secondary">Batal</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php'); ?>

    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    </script>

</body>

</html>