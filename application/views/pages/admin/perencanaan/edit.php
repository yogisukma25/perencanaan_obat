<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/admin/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Edit Perencanaan</h4>
                                        <a href="<?=site_url('perencanaan')?>"
                                            class="btn btn-secondary btn-sm">Kembali</a>
                                    </div>
                                    <div class="card-body">
                                        <?php echo validation_errors(); ?>
                                        <?php echo form_open('perencanaan/edit/' . $perencanaan->id); ?>
                                        <div class="form-group mb-3">
                                            <label for="puskesmas_id">Puskesmas</label>
                                            <select class="form-control" id="puskesmas_id" name="puskesmas_id">
                                                <?php foreach ($puskesmas as $p): ?>
                                                <option value="<?=$p->id?>"
                                                    <?=($p->id == $perencanaan->puskesmas_id) ? 'selected' : ''?>>
                                                    <?=$p->nama_puskesmas?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="nama_unit">Nama Unit</label>
                                            <input type="text" class="form-control" id="nama_unit" name="nama_unit"
                                                value="<?=set_value('nama_unit', $perencanaan->nama_unit)?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="kode_rekening">Kode Rekening</label>
                                            <input type="text" class="form-control" id="kode_rekening"
                                                name="kode_rekening"
                                                value="<?=set_value('kode_rekening', $perencanaan->kode_rekening)?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="sumber_dana">Sumber Dana</label>
                                            <select class="form-control" id="sumber_dana" name="sumber_dana">
                                                <option value="JKN " <?php if ($perencanaan->sumber_dana == "JKN (*)") {
    echo "selected";
}
?>>
                                                    JKN </option>
                                                <option value="RAWAT INAP" <?php if ($perencanaan->sumber_dana == "RAWAT INAP(*)") {
    echo "selected";
}
?>>
                                                    RAWAT INAP</option>
                                                <option value="PROGRAM" <?php if ($perencanaan->sumber_dana == "PROGRAM") {
    echo "selected";
}
?>>
                                                    PROGRAM</option>
                                                <option value="APBD" <?php if ($perencanaan->sumber_dana == "APBD") {
    echo "selected";
}
?>>
                                                    APBD</option>
                                            </select>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="tanggal_perencanaan">Tanggal Perencanaan</label>
                                            <input type="date" class="form-control" id="tanggal_perencanaan"
                                                name="tanggal_perencanaan"
                                                value="<?=set_value('tanggal_perencanaan', $perencanaan->tanggal_perencanaan)?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="status">Status</label>
                                            <select type="text" class="form-control" id="status" name="status">
                                                <option
                                                    value="<? if($perencanaan->status == 'pending'){echo('selected');}?>">
                                                    Pending
                                                </option>
                                                <option
                                                    value="<? if($perencanaan->status == 'diterima'){echo('selected');}?>">
                                                    Diterima
                                                </option>
                                                <option
                                                    value="<? if($perencanaan->status == 'ditolak'){echo('selected');}?>">
                                                    Ditolak
                                                </option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <a href="<?=site_url('perencanaan')?>" class="btn btn-secondary">Batal</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>


    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    </script>

</body>

</html>