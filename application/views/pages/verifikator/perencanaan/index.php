<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/verifikator/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="container-xxl flex-grow-1 container-p-y">
                        <!-- Notifikasi Sukses -->
                        <?php if ($this->session->flashdata('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?=$this->session->flashdata('success');?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        <?php endif;?>
                        <!-- Notifikasi Error -->
                        <?php if ($this->session->flashdata('error')): ?>
                        <div class="alert alert-error alert-dismissible fade show" role="alert">
                            <?=$this->session->flashdata('error');?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        <?php endif;?>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title mb-3 mt-2">Daftar Perencanaan</h4>
                                        <!-- <a href="<?=site_url('perencanaanpuskesmas/create')?>"
                                            class="btn btn-sm btn-primary">Tambah</a> -->
                                    </div>
                                    <div class="card-body">
                                        <table class="table" id="dataTable">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Sumber Dana</th>
                                                    <th>Tanggal Perencanaan</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($perencanaan as $index => $p): ?>
                                                <tr>
                                                    <td><?=$index + 1?></td>
                                                    <td><?=$p->sumber_dana?></td>
                                                    <td><?=$p->tanggal_perencanaan?></td>
                                                    <td><?=$p->status?></td>
                                                    <td>
                                                        <a href="<?=site_url('perencanaanverifikator/view/' . $p->id)?>"
                                                            class="btn btn-sm btn-primary">Rincian</a>
                                                        <!-- <a href="<?=site_url('perencanaanverifikator/approve/' . $p->id)?>"
                                                            class="btn btn-sm btn-success">Setujui</a>
                                                        <a href="<?=site_url('perencanaanverifikator/reject/' . $p->id)?>"
                                                            class="btn btn-sm btn-danger">Ditolak</a> -->
                                                    </td>
                                                </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>

    <!-- Modal untuk Mengisi Alasan Ditolak -->
    <div class="modal fade" id="rejectModal" tabindex="-1" aria-labelledby="rejectModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="rejectModalLabel">Alasan Penolakan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="rejectForm" method="post" action="">
                        <div class="mb-3">
                            <label for="rejectReason" class="form-label">Alasan</label>
                            <textarea class="form-control" id="rejectReason" name="reason" rows="3" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-danger">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });

    document.addEventListener('DOMContentLoaded', function() {
        var rejectButtons = document.querySelectorAll('.btn-danger');
        rejectButtons.forEach(function(button) {
            button.addEventListener('click', function(event) {
                event.preventDefault();
                var rejectUrl = button.getAttribute('href');
                var form = document.getElementById('rejectForm');
                form.setAttribute('action', rejectUrl);
                var modal = new bootstrap.Modal(document.getElementById('rejectModal'));
                modal.show();
            });
        });
    });
    </script>
</body>

</html>