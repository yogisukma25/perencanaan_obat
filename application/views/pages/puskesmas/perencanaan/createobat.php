<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/puskesmas/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Tambah Rincian Perencanaan</h4>
                                        <a href="<?=site_url('perencanaanpuskesmas/view/' . $perencanaan_id)?>"
                                            class="btn btn-sm btn-secondary">Kembali</a>
                                    </div>
                                    <div class="card-body">
                                        <?php echo validation_errors(); ?>
                                        <?php echo form_open('perencanaanpuskesmas/create_obat/' . $perencanaan_id); ?>
                                        <div class="form-group mb-3">
                                            <label for="obat_id">Obat</label>
                                            <select class="form-control" id="obat_id" name="obat_id">
                                                <?php foreach ($obat as $o): ?>
                                                <option value="<?=$o->id?>"><?=$o->nama_obat?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="keterangan">Keterangan</label>
                                            <input type="text" class="form-control" id="keterangan" name="keterangan"
                                                value="<?=set_value('keterangan')?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="jumlah">Jumlah</label>
                                            <input type="number" class="form-control" id="jumlah" name="jumlah"
                                                value="<?=set_value('jumlah')?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="satuan">satuan</label>
                                            <select class="form-control" id="satuan" name="satuan">
                                                <option value="PCS">
                                                    PCS
                                                </option>
                                                <option value="Unit">
                                                    Unit
                                                </option>
                                                <option value="Botol">
                                                    Botol
                                                </option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                        <a href="<?=site_url('perencanaanpuskesmas')?>"
                                            class="btn btn-secondary">Batal</a>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>

</body>

</html>