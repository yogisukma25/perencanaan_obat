<!DOCTYPE html>
<html lang="<?=str_replace('_', '-', $this->config->item('language'))?>" class="light-style layout-menu-fixed" dir="ltr"
    data-theme="theme-default" data-assets-path="<?=base_url('assets/template/')?>"
    data-template="vertical-menu-template-free">

<head>
    <?php $this->load->view('includes/pages/meta.php');?>
    <?php $this->load->view('includes/pages/style.php');?>
</head>

<body>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            <?php $this->load->view('includes/pages/puskesmas/sidebar.php');?>

            <div class="layout-page">

                <?php $this->load->view('includes/pages/navbar.php');?>

                <div class="content-wrapper">

                    <div class="content-backdrop fade"></div>

                    <div class="content-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Buat Perencanaan</h4>
                                        <a href="<?=site_url('perencanaanpuskesmas')?>"
                                            class="btn btn-secondary btn-sm">Kembali</a>
                                    </div>
                                    <div class="card-body">
                                        <?php echo validation_errors(); ?>
                                        <?php echo form_open('perencanaanpuskesmas/create'); ?>
                                        <div class="form-group mb-3" hidden>
                                            <label for="puskesmas_id">Puskesmas</label>
                                            <?php $this->load->library('session'); ?>
                                            <input type="text" class="form-control" id="puskesmas_id"
                                                name="puskesmas_id"
                                                value="<?php echo($data_session = $this->session->userdata('puskesmas_id'));?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="nama_unit">Nama Unit</label>
                                            <input type="text" class="form-control" id="nama_unit" name="nama_unit"
                                                value="<?=set_value('nama_unit')?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="kode_rekening">Kode Rekening</label>
                                            <input type="text" class="form-control" id="kode_rekening"
                                                name="kode_rekening" value="<?=set_value('kode_rekening')?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="sumber_dana">Sumber Dana</label>
                                            <select class="form-control" id="sumber_dana" name="sumber_dana">
                                                <?php if (!in_array('JKN (*)', $sumber_dana)) { ?>
                                                <option value="JKN (*)">JKN (*)</option>
                                                <?php } else { ?>
                                                <option value="JKN (*)" disabled>JKN (*) - Sudah Digunakan</option>
                                                <?php } ?>
                                                <?php if (!in_array('RAWAT INAP(*)', $sumber_dana)) { ?>
                                                <option value="RAWAT INAP(*)">RAWAT INAP(*)</option>
                                                <?php } else { ?>
                                                <option value="RAWAT INAP(*)" disabled>RAWAT INAP(*) - Sudah Digunakan
                                                </option>
                                                <?php } ?>
                                                <?php if (!in_array('PROGRAM', $sumber_dana)) { ?>
                                                <option value="PROGRAM">PROGRAM</option>
                                                <?php } else { ?>
                                                <option value="PROGRAM" disabled>PROGRAM - Sudah Digunakan</option>
                                                <?php } ?>
                                                <?php if (!in_array('APBD', $sumber_dana)) { ?>
                                                <option value="APBD">APBD</option>
                                                <?php } else { ?>
                                                <option value="APBD" disabled>APBD - Sudah Digunakan</option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                        <div class="form-group mb-3">
                                            <label for="tanggal_perencanaan">Tanggal Perencanaan</label>
                                            <input type="date" class="form-control" id="tanggal_perencanaan"
                                                name="tanggal_perencanaan"
                                                value="<?=set_value('tanggal_perencanaan')?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="status">Status</label>
                                            <input type="text" class="form-control" id="status" name="status"
                                                value="Direncanakan" readonly>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <a href="<?=site_url('perencanaanpuskesmas')?>"
                                            class="btn btn-secondary">Batal</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="layout-overlay layout-menu-toggle"></div>

    </div>
    <div class="open-menu" id="to-top" style="display: none;">
        <a class="text-white btn-open-menu"
            style="background-color: #a855f7; padding: 5px; height: 40px; width: 40px; border-radius: 50%; display: flex; align-items: center; justify-content: center;"
            id="scrollButton">
            <small><i class="mdi mdi-chevron-up"></i></small>
        </a>
    </div>
    <?php $this->load->view('includes/pages/script.php');?>


    <script>
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var button = document.getElementById("to-top");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }
    }

    document.getElementById("scrollButton").addEventListener("click", function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
    </script>

</body>

</html>