<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model yang diperlukan
        $this->load->model('Perencanaan_model');
        $this->load->model('Pengajuan_model');
        $this->load->model('Puskesmas_model');
        $this->load->model('User_model');
    }

    public function index()
    {
        // Mendapatkan data jumlah perencanaan dan pengajuan per puskesmas untuk bulan ini
        $data['statistics'] = $this->Puskesmas_model->get_all_puskesmas_grafik();

        // Tampilkan view dashboard dengan data statistik
        // $this->load->view('dashboard/index', $data);

        $role = $this->session->userdata('role');
        if ($role == 1) {
            $this->load->view('pages/admin/dashboard/index', $data);
        } elseif ($role == 3) {
            $this->load->view('pages/puskesmas/dashboard/index', $data);
        } elseif ($role == 2) {
            $this->load->view('pages/verifikator/dashboard/index', $data);
        }
    }

    private function get_statistics()
    {
        // Ambil data puskesmas
        $statistics =
            var_dump($statistics);
        return $statistics;
    }
}
