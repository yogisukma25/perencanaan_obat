<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Puskesmas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Puskesmas_model');
    }

    public function index()
    {
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $this->load->view('pages/admin/puskesmas/index', $data);
    }

    public function view($id)
    {
        $data['puskesmas'] = $this->Puskesmas_model->get_puskesmas_by_id($id);
        $this->load->view('pages/admin/puskesmas/view', $data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $data = array(
                'nama_puskesmas' => $this->input->post('nama_puskesmas'),
                // 'alamat' => $this->input->post('alamat'),
                // 'telepon' => $this->input->post('telepon'),
            );
            $this->Puskesmas_model->insert_puskesmas($data);
            $this->session->set_flashdata('success', 'Data puskesmas berhasil ditambah.');
            redirect('puskesmas');
        } else {
            $this->load->view('pages/admin/puskesmas/create');
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $data = array(
                'nama_puskesmas' => $this->input->post('nama_puskesmas'),
                // 'alamat' => $this->input->post('alamat'),
                // 'telepon' => $this->input->post('telepon'),
            );
            $this->Puskesmas_model->update_puskesmas($id, $data);
            $this->session->set_flashdata('success', 'Data puskesmas berhasil diubah.');
            redirect('puskesmas');
        } else {
            $data['puskesmas'] = $this->Puskesmas_model->get_puskesmas_by_id($id);
            $this->load->view('pages/admin/puskesmas/edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Puskesmas_model->delete_puskesmas($id);
        $this->session->set_flashdata('success', 'Data puskesmas berhasil dihapus.');
        redirect('puskesmas');
    }
}
