<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PerencanaanVerifikator extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Perencanaan_model');
        $this->load->model('Puskesmas_model');
        $this->load->model('Detail_perencanaan_model');
        $this->load->model('Obat_model');
    }

    public function index()
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_all_perencanaan();
        // var_dump($data);
        $this->load->view('pages/verifikator/perencanaan/index', $data);
    }

    public function view($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_all_detail_perencanaan($id);
        $this->load->view('pages/verifikator/perencanaan/view', $data);
    }

    public function create()
    {
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_perencanaan', 'Nama Perencanaan', 'required');
        $this->form_validation->set_rules('nama_program', 'Nama Program', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/verifikator/perencanaan/create', $data);
        } else {
            $perencanaan_data = array(
                'puskesmas_id' => $this->input->post('puskesmas_id'),
                'nama_perencanaan' => $this->input->post('nama_perencanaan'),
                'nama_program' => $this->input->post('nama_program'),
                'nama_unit' => $this->input->post('nama_unit'),
                'kode_rekening' => $this->input->post('kode_rekening'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => 'Direncanakan',
            );

            $this->Perencanaan_model->insert_perencanaan($perencanaan_data);
            $perencanaan_id = $this->db->insert_id();

            $obat_ids = $this->input->post('obat_id');
            $jumlahs = $this->input->post('jumlah');
            foreach ($obat_ids as $index => $obat_id) {
                $detail_data = array(
                    'perencanaan_id' => $perencanaan_id,
                    'obat_id' => $obat_id,
                    'jumlah' => $jumlahs[$index],
                );
                $this->Detail_perencanaan_model->insert_detail_perencanaan($detail_data);
            }

            redirect('perencanaanverifikator');
        }
    }

    public function edit($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_perencanaan', 'Nama Perencanaan', 'required');
        $this->form_validation->set_rules('nama_program', 'Nama Program', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/verifikator/perencanaan/edit', $data);
        } else {
            $perencanaan_data = array(
                'puskesmas_id' => $this->input->post('puskesmas_id'),
                'nama_perencanaan' => $this->input->post('nama_perencanaan'),
                'nama_program' => $this->input->post('nama_program'),
                'nama_unit' => $this->input->post('nama_unit'),
                'kode_rekening' => $this->input->post('kode_rekening'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => $this->input->post('status'),
            );

            $this->Perencanaan_model->update_perencanaan($id, $perencanaan_data);

            $this->Detail_perencanaan_model->delete_detail_by_perencanaan_id($id);
            $obat_ids = $this->input->post('obat_id');
            $jumlahs = $this->input->post('jumlah');
            foreach ($obat_ids as $index => $obat_id) {
                $detail_data = array(
                    'perencanaan_id' => $id,
                    'obat_id' => $obat_id,
                    'jumlah' => $jumlahs[$index],
                );
                $this->Detail_perencanaan_model->insert_detail_perencanaan($detail_data);
            }

            redirect('perencanaanverifikator');
        }
    }

    public function delete($id)
    {
        $this->Perencanaan_model->delete_perencanaan($id);
        $this->Detail_perencanaan_model->delete_detail_by_perencanaan_id($id);
        redirect('perencanaanverifikator');
    }

    public function approve($id)
    {
        $perencanaan_data = array(
            'status' => 'Disetujui',
        );
        $this->Perencanaan_model->update_perencanaan($id, $perencanaan_data);
        $this->session->set_flashdata('success', 'Data perencanaan berhasil disetujui.');
        redirect('pengajuanverifikator');
    }
    public function reject($id)
    {
        $perencanaan_data = array(
            'status' => 'Ditolak',
        );
        $this->Perencanaan_model->update_perencanaan($id, $perencanaan_data);
        $this->session->set_flashdata('success', 'Data perencanaan berhasil ditolak.');
        redirect('pengajuanverifikator');
    }

}
