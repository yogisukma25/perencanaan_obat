<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perencanaan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Perencanaan_model');
        $this->load->model('Puskesmas_model');
        $this->load->model('Detail_perencanaan_model');
        $this->load->model('Obat_model');
    }

    public function index()
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_all_perencanaan();
        $this->load->view('pages/admin/perencanaan/index', $data);
    }

    public function view($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $this->load->view('pages/admin/perencanaan/view', $data);
    }

    public function create()
    {
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('sumber_dana', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/admin/perencanaan/create', $data);
        } else {
            $puskesmas_id = $this->input->post('puskesmas_id');
            $sumber_dana = $this->input->post('sumber_dana');
            $tahun = date('Y', strtotime($this->input->post('tanggal_perencanaan')));

            // Asumsi $this->Puskesmas_model memiliki metode get_puskesmas_by_id
            $puskesmas = $this->Puskesmas_model->get_puskesmas_by_id($puskesmas_id);

            // Membuat nota dinamis
            $nota = "RKO/{$puskesmas->nama_puskesmas}/{$sumber_dana}/{$tahun}";

            $perencanaan_data = array(
                'puskesmas_id' => $puskesmas_id,
                'nama_unit' => $this->input->post('nama_unit'),
                'nota' => $nota,
                'kode_rekening' => $this->input->post('kode_rekening'),
                'sumber_dana' => $this->input->post('sumber_dana'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => 'Direncanakan',
            );

            $this->Perencanaan_model->insert_perencanaan($perencanaan_data);
            $this->session->set_flashdata('success', 'Data perencanaan berhasil ditambah.');
            redirect('perencanaan');
        }
    }

    public function edit($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('sumber_dana', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/admin/perencanaan/edit', $data);
        } else {

            $puskesmas_id = $this->input->post('puskesmas_id');
            $sumber_dana = $this->input->post('sumber_dana');
            $tahun = date('Y', strtotime($this->input->post('tanggal_perencanaan')));

            // Asumsi $this->Puskesmas_model memiliki metode get_puskesmas_by_id
            $puskesmas = $this->Puskesmas_model->get_puskesmas_by_id($puskesmas_id);

            // Membuat nota dinamis
            $nota = "RKO/{$puskesmas->nama_puskesmas}/{$sumber_dana}/{$tahun}";
            $perencanaan_data = array(
                'puskesmas_id' => $puskesmas_id,
                'nama_unit' => $this->input->post('nama_unit'),
                'nota' => $nota,
                'kode_rekening' => $this->input->post('kode_rekening'),
                'sumber_dana' => $this->input->post('sumber_dana'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => 'Direncanakan',
            );

            $this->Perencanaan_model->update_perencanaan($id, $perencanaan_data);
            $this->session->set_flashdata('success', 'Data perencanaan berhasil diubah.');
            redirect('perencanaan');
        }
    }

    public function delete($id)
    {
        $this->Perencanaan_model->delete_perencanaan($id);
        $this->Detail_perencanaan_model->delete_detail_by_perencanaan_id($id);
        $this->session->set_flashdata('success', 'Data perencanaan berhasil dihapus.');
        redirect('perencanaan');
    }
}
