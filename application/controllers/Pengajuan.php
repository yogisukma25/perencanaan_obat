<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Pengajuan_model');
        $this->load->model('Perencanaan_model');
        $this->load->model('User_model');
    }

    public function index() {
        $data['pengajuan'] = $this->Pengajuan_model->get_all_pengajuan();
        $this->load->view('pages/admin/pengajuan/index', $data);
    }

    public function view($id) {
        $data['pengajuan'] = $this->Pengajuan_model->get_pengajuan_by_id($id);
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($data['pengajuan']->perencanaan_id);
        $data['verifikator'] = $this->User_model->get_user_by_id($data['pengajuan']->verifikator_id);
        $this->load->view('pages/admin/pengajuan/view', $data);
    }

    public function approve($id) {
        $pengajuan_data = array(
            'status' => 'Disetujui'
        );
        $this->Pengajuan_model->update_pengajuan($id, $pengajuan_data);
        redirect('pengajuan');
    }

    public function reject($id) {
        $pengajuan_data = array(
            'status' => 'Ditolak'
        );
        $this->Pengajuan_model->update_pengajuan($id, $pengajuan_data);
        redirect('pengajuan');
    }
}