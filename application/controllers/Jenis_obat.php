<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_obat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Jenis_obat_model');
    }

    public function index()
    {
        $data['jenis_obat'] = $this->Jenis_obat_model->get_all_jenis_obat();
        $this->load->view('pages/admin/jenis_obat/index', $data);
    }

    public function view($id)
    {
        $data['jenis_obat'] = $this->Jenis_obat_model->get_jenis_obat_by_id($id);
        $this->load->view('pages/admin/jenis_obat/view', $data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $data = array(
                'nama_jenis' => $this->input->post('nama_jenis'),
            );
            $this->Jenis_obat_model->insert_jenis_obat($data);
            $this->session->set_flashdata('success', 'Data jenis obat berhasil ditambah.');
            redirect('jenis_obat');
        } else {
            $this->load->view('pages/admin/jenis_obat/create');
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $data = array(
                'nama_jenis' => $this->input->post('nama_jenis'),
            );
            $this->Jenis_obat_model->update_jenis_obat($id, $data);
            $this->session->set_flashdata('success', 'Data jenis obat berhasil diubah.');
            redirect('jenis_obat');
        } else {
            $data['jenis_obat'] = $this->Jenis_obat_model->get_jenis_obat_by_id($id);
            $this->load->view('pages/admin/jenis_obat/edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Jenis_obat_model->delete_jenis_obat($id);
        redirect('jenis_obat');
    }
}