<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail_perencanaan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Detail_perencanaan_model');
        $this->load->model('Obat_model');
    }

    public function index()
    {
        $data['detail_perencanaan'] = $this->Detail_perencanaan_model->get_all_detail_perencanaan();
        $this->load->view('detail_perencanaan/index', $data);
    }

    public function view($id)
    {
        $data['detail_perencanaan'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $data['obat'] = $this->Obat_model->get_obat_by_id($data['detail_perencanaan']->obat_id);
        $this->load->view('detail_perencanaan/view', $data);
    }
}