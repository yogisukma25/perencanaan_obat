<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PerencanaanPuskesmas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Perencanaan_model');
        $this->load->model('Puskesmas_model');
        $this->load->model('Detail_perencanaan_model');
        $this->load->model('Obat_model');
    }

    public function index()
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_all_perencanaan_puskesmas();
        $this->load->view('pages/puskesmas/perencanaan/index', $data);
    }

    public function view($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_all_detail_perencanaan($id);
        // var_dump($data);
        $data['create_obat_url'] = site_url('perencanaanpuskesmas/create_obat/' . $id);
        $this->load->view('pages/puskesmas/perencanaan/view', $data);
    }

    public function create()
    {
        $data['sumber_dana'] = $this->Perencanaan_model->sumber_dana();
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('sumber_dana', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/puskesmas/perencanaan/create', $data);
        } else {
            $puskesmas_id = $this->input->post('puskesmas_id');
            $sumber_dana = $this->input->post('sumber_dana');
            $tahun = date('Y', strtotime($this->input->post('tanggal_perencanaan')));

            // Asumsi $this->Puskesmas_model memiliki metode get_puskesmas_by_id
            $puskesmas = $this->Puskesmas_model->get_puskesmas_by_id($puskesmas_id);

            // Membuat nota dinamis
            $nota = "RKO/{$puskesmas->nama_puskesmas}/{$sumber_dana}/{$tahun}";

            $perencanaan_data = array(
                'puskesmas_id' => $puskesmas_id,
                'nama_unit' => $this->input->post('nama_unit'),
                'nota' => $nota,
                'kode_rekening' => $this->input->post('kode_rekening'),
                'sumber_dana' => $this->input->post('sumber_dana'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => 'Direncanakan',
            );

            $this->Perencanaan_model->insert_perencanaan($perencanaan_data);
            $this->session->set_flashdata('success', 'Data perencanaan berhasil ditambah.');
            redirect('perencanaanpuskesmas');
        }
    }

    public function edit($id)
    {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
        $data['obat'] = $this->Obat_model->get_all_obat();

        $this->form_validation->set_rules('puskesmas_id', 'Puskesmas', 'required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'required');
        $this->form_validation->set_rules('kode_rekening', 'Kode Rekening', 'required');
        $this->form_validation->set_rules('sumber_dana', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('tanggal_perencanaan', 'Tanggal Perencanaan', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/puskesmas/perencanaan/edit', $data);
        } else {

            $puskesmas_id = $this->input->post('puskesmas_id');
            $sumber_dana = $this->input->post('sumber_dana');
            $tahun = date('Y', strtotime($this->input->post('tanggal_perencanaan')));

            // Asumsi $this->Puskesmas_model memiliki metode get_puskesmas_by_id
            $puskesmas = $this->Puskesmas_model->get_puskesmas_by_id($puskesmas_id);

            // Membuat nota dinamis
            $nota = "RKO/{$puskesmas->nama_puskesmas}/{$sumber_dana}/{$tahun}";
            $perencanaan_data = array(
                'puskesmas_id' => $puskesmas_id,
                'nama_unit' => $this->input->post('nama_unit'),
                'nota' => $nota,
                'kode_rekening' => $this->input->post('kode_rekening'),
                'sumber_dana' => $this->input->post('sumber_dana'),
                'tanggal_perencanaan' => $this->input->post('tanggal_perencanaan'),
                'status' => 'Direncanakan',
            );

            $this->Perencanaan_model->update_perencanaan($id, $perencanaan_data);
            $this->session->set_flashdata('success', 'Data perencanaan berhasil diubah.');
            redirect('perencanaanpuskesmas');
        }
    }

    public function delete($id)
    {
        $this->Perencanaan_model->delete_perencanaan($id);
        $this->Detail_perencanaan_model->delete_detail_by_perencanaan_id($id);
        $this->session->set_flashdata('success', 'Data perencanaan berhasil dihapus.');
        redirect('perencanaanpuskesmas');
    }

    public function create_obat($perencanaan_id)
    {
        $data['perencanaan_id'] = $perencanaan_id;
        $data['obat'] = $this->Obat_model->get_all_obat();

        // Validasi form untuk obat
        $this->form_validation->set_rules('obat_id', 'Obat', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        // $this->form_validation->set_rules('pakai', 'Pakai', 'required');
        // $this->form_validation->set_rules('rusak', 'Rusak', 'required');
        // $this->form_validation->set_rules('alasan_rusak', 'Alasan Rusak', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('pages/puskesmas/perencanaan/createobat', $data);
        } else {
            $obat_id = $this->input->post('obat_id');
            $jumlah = $this->input->post('jumlah');
            $keterangan = $this->input->post('keterangan');
            $satuan = $this->input->post('satuan');
            // $pakai = $this->input->post('pakai');
            // $rusak = $this->input->post('rusak');
            // $alasan_rusak = $this->input->post('alasan_rusak');
            $detail_data = array(
                'perencanaan_id' => $perencanaan_id,
                'obat_id' => $obat_id,
                'jumlah' => $jumlah,
                'keterangan' => $keterangan,
                'satuan' => $satuan,
            );
            $this->Detail_perencanaan_model->insert_detail_perencanaan($detail_data);
            $this->session->set_flashdata('success', 'Data obat perencanaan berhasil ditambah.');
            redirect('perencanaanpuskesmas');
        }
    }

    public function edit_obat($id,$perencanaan_id)
    {
        $data['perencanaan_id'] = $perencanaan_id;
        $data['detail'] = $this->Detail_perencanaan_model->get_detail_perencanaan_by_id($id);
        $data['obat'] = $this->Obat_model->get_all_obat(); // Ambil data obat untuk dropdown atau pilihan

        // Validasi form untuk obat
        $this->form_validation->set_rules('obat_id', 'Obat', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        // $this->form_validation->set_rules('pakai', 'Pakai', 'required');
        // $this->form_validation->set_rules('rusak', 'Rusak', 'required');
        // $this->form_validation->set_rules('alasan_rusak', 'Alasan Rusak', 'required');

        if ($this->form_validation->run() === false) {
            // var_dump($data);
            $this->load->view('pages/puskesmas/perencanaan/editobat', $data);
        } else {
            $obat_id = $this->input->post('obat_id');
            $jumlah = $this->input->post('jumlah');
            $keterangan = $this->input->post('keterangan');
            $satuan = $this->input->post('satuan');
            // $pakai = $this->input->post('pakai');
            // $rusak = $this->input->post('rusak');
            // $alasan_rusak = $this->input->post('alasan_rusak');
            $detail_data = array(
                'obat_id' => $obat_id,
                'jumlah' => $jumlah,
                'keterangan' => $keterangan,
                'satuan' => $satuan,
                // 'pakai' => $pakai,
                // 'rusak' => $rusak,
                // 'alasan_rusak' => $alasan_rusak,
            );
            $this->Detail_perencanaan_model->update_detail_perencanaan($id, $detail_data);
            $this->session->set_flashdata('success', 'Data obat perencanaan berhasil diubah.');
            redirect('perencanaanpuskesmas');
        }
    }

}