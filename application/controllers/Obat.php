<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Obat_model');
        // $this->load->model('Jenis_obat_model');
        $this->load->model('Standarisasi_harga_model');
    }

    public function index()
    {
        // $this->session->set_flashdata('success', 'Data berhasil diambil.');
        $data['obat'] = $this->Obat_model->get_all_obat();
        $this->load->view('pages/admin/obat/index', $data);
    }

    public function view($id)
    {
        $data['obat'] = $this->Obat_model->get_obat_by_id($id);
        $this->load->view('pages/admin/obat/view', $data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $data = array(
                'nama_obat' => $this->input->post('nama_obat'),
                'harga' => $this->input->post('harga'),
            );
            $obat_id = $this->Obat_model->insert_obat($data);
            // $data1 = array(
            //     'obat_id' => $obat_id,
            //     'harga' => $this->input->post('harga'),
            // );
            // $this->Standarisasi_harga_model->insert_standarisasi_harga($data1);
            $this->session->set_flashdata('success', 'Data obat berhasil ditambah.');
            redirect('obat');
        } else {
            // $data['jenis_obat'] = $this->Jenis_obat_model->get_all_jenis_obat();
            $this->load->view('pages/admin/obat/create');
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $data = array(
                'nama_obat' => $this->input->post('nama_obat'),
                'harga' => $this->input->post('harga'),
            );
            $this->Obat_model->update_obat($id, $data);
            $this->session->set_flashdata('success', 'Data obat berhasil diubah.');
            redirect('obat');
        } else {
            $data['obat'] = $this->Obat_model->get_obat_by_id($id);
            $this->load->view('pages/admin/obat/edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Obat_model->delete_obat($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus.');
        redirect('obat');
    }
}
