<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Role_model');
        $this->load->model('Puskesmas_model');
    }

    public function index()
    {
        $data['users'] = $this->User_model->get_all_users();
        $this->load->view('pages/admin/users/index', $data);
    }

    public function view($id)
    {
        $data['user'] = $this->User_model->get_user_by_id($id);
        $this->load->view('pages/admin/users/view', $data);
    }

    public function create()
    {
        if ($this->input->post('puskesmas_id') == '') {
        $puskesmas_id = NULL;
    }else{
        $puskesmas_id = $this->input->post('puskesmas_id');
    }
        if ($this->input->post()) {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                'role_id' => $this->input->post('role_id'),
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'email' => $this->input->post('email'),
                'puskesmas_id' => $puskesmas_id,
            );
            $this->User_model->insert_user($data);
            $this->session->set_flashdata('success', 'Data pengguna berhasil ditambah.');
            redirect('user');
        } else {
            $data['roles'] = $this->Role_model->get_all_roles();
            $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
            $this->load->view('pages/admin/users/create', $data);
        }
    }

    public function edit($id)
    {
        if ($this->input->post('puskesmas_id') == '') {
            $puskesmas_id = NULL;
        }else{
            $puskesmas_id = $this->input->post('puskesmas_id');
        }
        if ($this->input->post()) {
            $data = array(
                'username' => $this->input->post('username'),
                'role_id' => $this->input->post('role_id'),
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'email' => $this->input->post('email'),
                'puskesmas_id' => $puskesmas_id,
            );
            if ($this->input->post('password')) {
                $data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
            }
            $this->User_model->update_user($id, $data);
            $this->session->set_flashdata('success', 'Data pengguna berhasil diubah.');
            redirect('user');
        } else {
            $data['user'] = $this->User_model->get_user_by_id($id);
            $data['roles'] = $this->Role_model->get_all_roles();
            $data['puskesmas'] = $this->Puskesmas_model->get_all_puskesmas();
            $this->load->view('pages/admin/users/edit', $data);
        }
    }

    public function delete($id)
    {
        $this->User_model->delete_user($id);
        $this->session->set_flashdata('success', 'Data pengguna berhasil dihapus.');
        redirect('user');
    }
}