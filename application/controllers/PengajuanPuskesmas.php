<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajuanPuskesmas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Pengajuan_model');
        $this->load->model('Perencanaan_model');
        $this->load->model('User_model');
    }

    public function index() {
        $data['pengajuan'] = $this->Pengajuan_model->get_all_pengajuan_puskesmas();
        $this->load->view('pages/puskesmas/pengajuan/index', $data);
    }

    public function view($id) {
        $data['perencanaan'] = $this->Perencanaan_model->get_perencanaan_by_id($id);
        $data['details'] = $this->Detail_perencanaan_model->get_all_detail_perencanaan($id);
        // var_dump($data);
        $data['create_obat_url'] = site_url('perencanaanpuskesmas/create_obat/' . $id);
        $this->load->view('pages/puskesmas/pengajuan/view', $data);
    }

    public function approve($id) {
        $pengajuan_data = array(
            'status' => 'Disetujui'
        );
        $this->Pengajuan_model->update_pengajuan($id, $pengajuan_data);
        redirect('pengajuanpuskesmas');
    }

    public function reject($id) {
        $pengajuan_data = array(
            'status' => 'Ditolak'
        );
        $this->Pengajuan_model->update_pengajuan($id, $pengajuan_data);
        redirect('pengajuanpuskesmas');
    }
}