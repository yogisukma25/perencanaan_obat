<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model yang diperlukan
        $this->load->model('User_model');
    }

    public function index()
    {
        // Jika user sudah login, redirect ke halaman lain (misalnya dashboard)
        if ($this->session->userdata('logged_in')) {
            redirect('/dashboard');
        }

        // Tampilkan halaman login
        $this->load->view('auth/login');
    }

    public function process()
    {
        // Validasi form
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === false) {
            // Jika validasi gagal, kembali ke halaman login
            $this->load->view('auth/login');
        } else {
            // Ambil input dari form
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            // Panggil model untuk cek login
            $user = $this->User_model->get_user_by_email($email);

            if ($user && password_verify($password, $user->password)) {
                // Buat session untuk user yang sudah login
                $user_data = array(
                    'user_id' => $user->id,
                    'nama_lengkap' => $user->nama_lengkap,
                    'email' => $user->email,
                    'role' => $user->role_id,
                    'puskesmas_id' => $user->puskesmas_id,
                    'logged_in' => true,
                );

                $this->session->set_userdata($user_data);

                if ($this->session->userdata('role') == 1) {
                    redirect('dashboardadmin');
                } elseif ($this->session->userdata('role') == 3) {
                    redirect('dashboardpuskesmas');
                } elseif ($this->session->userdata('role') == 2) {
                    redirect('dashboardverifikator');
                }
                // Redirect ke halaman dashboard atau halaman lain setelah login sukses
            } else {
                // Jika login gagal, tampilkan pesan error
                $data['error'] = 'Email atau password salah.';
                $this->load->view('auth/login', $data);
            }
        }
    }

    public function logout()
    {
        // Hapus semua data session
        $this->session->sess_destroy();

        // Redirect ke halaman login setelah logout
        redirect('/');
    }
}