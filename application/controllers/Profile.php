<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model yang diperlukan
        $this->load->model('User_model');
    }

    public function index()
    {
        // Mendapatkan data user dari session atau model lainnya
        $user_id = $this->session->userdata('user_id');
        $data['user'] = $this->User_model->get_user_by_id($user_id);

        // Tampilkan view untuk profile
        $this->load->view('pages/admin/profile/index', $data);
    }

    public function edit()
    {
        // Mendapatkan data user dari session atau model lainnya
        $user_id = $this->session->userdata('user_id');
        $data['user'] = $this->User_model->get_user_by_id($user_id);

        // Validasi form
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'matches[password]');

        if ($this->form_validation->run() === false) {
            // Jika validasi gagal, tampilkan form edit
            $this->load->view('pages/admin/profile/edit', $data);
        } else {
            // Jika validasi berhasil, update data user
            $update_data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'email' => $this->input->post('email'),
            );

            // Update password jika ada perubahan
            $password = $this->input->post('password');
            if (!empty($password)) {
                $update_data['password'] = password_hash($password, PASSWORD_DEFAULT);
            }

            // Panggil model untuk update data user
            $this->User_model->update_user($user_id, $update_data);

            // Redirect kembali ke halaman profile
            redirect('profile');
        }
    }
}