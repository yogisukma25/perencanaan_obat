<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Role_model');
    }

    public function index()
    {
        $data['roles'] = $this->Role_model->get_all_roles();
        $this->load->view('pages/admin/roles/index', $data);
    }

    public function view($id)
    {
        $data['role'] = $this->Role_model->get_role_by_id($id);
        $this->load->view('pages/admin/roles/view', $data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $data = array(
                'role_name' => $this->input->post('role_name'),
            );
            $this->Role_model->insert_role($data);
            $this->session->set_flashdata('success', 'Data role berhasil ditambah.');
            redirect('role');
        } else {
            $this->load->view('pages/admin/roles/create');
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $data = array(
                'role_name' => $this->input->post('role_name'),
            );
            $this->Role_model->update_role($id, $data);
            $this->session->set_flashdata('success', 'Data role berhasil diubah.');
            redirect('role');
        } else {
            $data['role'] = $this->Role_model->get_role_by_id($id);
            $this->load->view('pages/admin/roles/edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Role_model->delete_role($id);
        $this->session->set_flashdata('success', 'Data role berhasil dihapus.');
        redirect('role');
    }
}
