<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;

// Route untuk halaman login
$route['login'] = 'login/index';
$route['login/process'] = 'login/process';
$route['login/logout'] = 'login/logout';

//dashboardadmin
$route['dashboardadmin'] = 'dashboard/index';
//dashboardpuskesmas
$route['dashboardpuskesmas'] = 'dashboard/index';
//dashboardverifikator
$route['dashboardverifikator'] = 'dashboard/index';

// User
$route['user'] = 'user/index';
$route['user/create'] = 'user/create';
$route['user/store'] = 'user/store';
$route['user/edit/(:any)'] = 'user/edit/$1';
$route['user/update/(:any)'] = 'user/update/$1';
$route['user/delete/(:any)'] = 'user/delete/$1';

// Puskesmas
$route['puskesmas'] = 'puskesmas/index';
$route['puskesmas/create'] = 'puskesmas/create';
$route['puskesmas/store'] = 'puskesmas/store';
$route['puskesmas/edit/(:any)'] = 'puskesmas/edit/$1';
$route['puskesmas/update/(:any)'] = 'puskesmas/update/$1';
$route['puskesmas/delete/(:any)'] = 'puskesmas/delete/$1';

// Jenis Obat
$route['jenis_obat'] = 'jenis_obat/index';
$route['jenis_obat/create'] = 'jenis_obat/create';
$route['jenis_obat/store'] = 'jenis_obat/store';
$route['jenis_obat/edit/(:any)'] = 'jenis_obat/edit/$1';
$route['jenis_obat/update/(:any)'] = 'jenis_obat/update/$1';
$route['jenis_obat/delete/(:any)'] = 'jenis_obat/delete/$1';

// Obat
$route['obat'] = 'obat/index';
$route['obat/create'] = 'obat/create';
$route['obat/store'] = 'obat/store';
$route['obat/edit/(:any)'] = 'obat/edit/$1';
$route['obat/update/(:any)'] = 'obat/update/$1';
$route['obat/delete/(:any)'] = 'obat/delete/$1';

// Perencanaan
$route['perencanaan'] = 'perencanaan/index';
$route['perencanaan/create'] = 'perencanaan/create';
$route['perencanaan/store'] = 'perencanaan/store';
$route['perencanaan/edit/(:any)'] = 'perencanaan/edit/$1';
$route['perencanaan/update/(:any)'] = 'perencanaan/update/$1';
$route['perencanaan/delete/(:any)'] = 'perencanaan/delete/$1';

// Perencanaan Puskesmas
$route['perencanaanpuskesmas'] = 'perencanaanpuskesmas/index';
$route['perencanaanpuskesmas/create'] = 'perencanaanpuskesmas/create';
$route['perencanaanpuskesmas/store'] = 'perencanaanpuskesmas/store';
$route['perencanaanpuskesmas/edit/(:any)'] = 'perencanaanpuskesmas/edit/$1';
$route['perencanaanpuskesmas/update/(:any)'] = 'perencanaanpuskesmas/update/$1';
$route['perencanaanpuskesmas/delete/(:any)'] = 'perencanaanpuskesmas/delete/$1';
$route['perencanaanpuskesmas/create_obat/(:num)'] = 'perencanaanpuskesmas/create_obat/$1';
$route['perencanaanpuskesmas/edit_obat/(:num)'] = 'perencanaanpuskesmas/edit_obat/$1';

// Perencanaan Verifikator
$route['perencanaanverifikator'] = 'perencanaanverifikator/index';
$route['perencanaanverifikator/approve/(:any)'] = 'perencanaanverifikator/approve/$1';
$route['perencanaanverifikator/reject/(:any)'] = 'perencanaanverifikator/reject/$1';
$route['perencanaanverifikator/delete/(:any)'] = 'perencanaanverifikator/delete/$1';

// Pengajuan
$route['pengajuan'] = 'pengajuan/index';

// Pengajuan Puskesmas
$route['pengajuanpuskesmas'] = 'pengajuanpuskesmas/index';

// Pengajuan verifikator
$route['pengajuanverifikator'] = 'pengajuanverifikator/index';

// Profile
$route['profile'] = 'profile/index';
$route['profile/edit'] = 'profile/edit';

// Logout
$route['auth/logout'] = 'auth/logout';
