<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standarisasi_harga_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function get_all_standarisasi_harga() {
        $this->db->select('standarisasi_harga.*, obat.nama_obat');
        $this->db->from('standarisasi_harga');
        $this->db->join('obat', 'standarisasi_harga.obat_id = obat.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_standarisasi_harga_by_id($id) {
        $query = $this->db->get_where('standarisasi_harga', array('id' => $id));
        return $query->row();
    }

    public function insert_standarisasi_harga($data) {
        return $this->db->insert('standarisasi_harga', $data);
    }

    public function update_standarisasi_harga($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('standarisasi_harga', $data);
    }

    public function delete_standarisasi_harga($id) {
        $this->db->where('id', $id);
        return $this->db->delete('standarisasi_harga');
    }
}