<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajuan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_pengajuan()
    {
        $this->db->select('perencanaan.*, puskesmas.nama_puskesmas');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('status', 'Disetujui');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_pengajuan_puskesmas()
    {
        $id = $this->session->userdata('puskesmas_id');
        $this->db->select('perencanaan.*, puskesmas.nama_puskesmas');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('status', 'Disetujui');
        $this->db->where('puskesmas_id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_pengajuan_day()
    {
        $this->db->select('perencanaan.*, puskesmas.nama_puskesmas');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('status', 'Disetujui');
        $this->db->where('DATE(perencanaan.tanggal_perencanaan)', 'CURDATE()', FALSE); // Sesuaikan 'tanggal' dengan nama kolom yang sesuai
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pengajuan_by_id($id)
    {
        $query = $this->db->get_where('pengajuan', array('id' => $id));
        return $query->row();
    }
    public function get_pengajuan_by_day($id)
    {
        $this->db->where('DATE(tanggal_perencanaan)', 'CURDATE()', FALSE); // Sesuaikan 'tanggal' dengan nama kolom di tabel Anda
        $this->db->where('id', $id);
        $query = $this->db->get('pengajuan');
        return $query->row();
    }

    public function insert_pengajuan($data)
    {
        return $this->db->insert('pengajuan', $data);
    }

    public function update_pengajuan($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('pengajuan', $data);
    }

    public function delete_pengajuan($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('pengajuan');
    }
}