<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perencanaan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_perencanaan()
    {
        $this->db->select('perencanaan.*, puskesmas.nama_puskesmas');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('perencanaan.status !=', 'Disetujui');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_perencanaan_puskesmas()
    {
        $id = $this->session->userdata('puskesmas_id');
        $this->db->select('perencanaan.*, puskesmas.nama_puskesmas');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('perencanaan.puskesmas_id', $id); // Menambahkan kondisi where untuk menyaring berdasarkan puskesmas_id
        $query = $this->db->get();
        return $query->result();
    }
    
    public function sumber_dana()
    {
        $id = $this->session->userdata('puskesmas_id');
        
        // Memastikan id puskesmas tersedia di session
        if (!$id) {
            return []; // Mengembalikan array kosong jika id puskesmas tidak ditemukan
        }
        
        // Menghitung tahun kemarin
        $tahun_kemarin = date('Y', strtotime('-1 year'));
        
        $this->db->select('perencanaan.sumber_dana');
        $this->db->from('perencanaan');
        $this->db->join('puskesmas', 'perencanaan.puskesmas_id = puskesmas.id');
        $this->db->where('perencanaan.puskesmas_id', $id); // Menyaring berdasarkan puskesmas_id
        $this->db->where('YEAR(perencanaan.tanggal_perencanaan)', $tahun_kemarin); // Menyaring berdasarkan tahun tanggal_perencanaan
        
        $query = $this->db->get();
        
        // Memastikan query berhasil dijalankan
        if ($query->num_rows() > 0) {
            return array_column($query->result_array(), 'sumber_dana');
        } else {
            return []; // Mengembalikan array kosong jika tidak ada hasil
        }
    }



    public function get_perencanaan_by_id($id)
    {
        $query = $this->db->get_where('perencanaan', array('id' => $id));
        return $query->row();
    }

    public function insert_perencanaan($data)
    {
        $this->db->insert('perencanaan', $data);
        return $this->db->insert_id();
    }

    public function update_perencanaan($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('perencanaan', $data);
    }

    public function delete_perencanaan($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('perencanaan');
    }
    
}