<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obat_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_obat()
    {
        $this->db->select('*');
        // $this->db->select('obat.*, jenis_obat.nama_jenis, standarisasi_harga.harga as harga_standarisasi');
        $this->db->from('obat');
        // $this->db->join('jenis_obat', 'obat.jenis_id = jenis_obat.id');
        // $this->db->join('standarisasi_harga', 'standarisasi_harga.obat_id = obat.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_obat_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('obat');
        $this->db->where('obat.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function insert_obat($data)
    {
        $this->db->insert('obat', $data);
        return $this->db->insert_id();
    }

    public function update_obat($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('obat', $data);
    }

    public function delete_obat($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('obat');
    }
}
