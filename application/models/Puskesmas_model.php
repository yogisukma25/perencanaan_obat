<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Puskesmas_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_puskesmas_grafik()
    {
        $this->db->select('p.nama_puskesmas, COALESCE(SUM(o.harga), 0) AS total_nominal');
        $this->db->from('puskesmas p');
        $this->db->join('perencanaan pr', 'p.id = pr.puskesmas_id', 'left');
        $this->db->join('detail_perencanaan d', 'pr.id = d.perencanaan_id', 'left');
        $this->db->join('obat o', 'd.obat_id = o.id', 'left');
        $this->db->group_by('p.nama_puskesmas');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_all_puskesmas()
    {
        $query = $this->db->get('puskesmas');
        return $query->result();
    }

    public function get_puskesmas_by_id($id)
    {
        $query = $this->db->get_where('puskesmas', array('id' => $id));
        return $query->row();
    }

    public function insert_puskesmas($data)
    {
        return $this->db->insert('puskesmas', $data);
    }

    public function update_puskesmas($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('puskesmas', $data);
    }

    public function delete_puskesmas($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('puskesmas');
    }
}
