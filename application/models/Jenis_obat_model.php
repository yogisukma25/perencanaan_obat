<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_obat_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function get_all_jenis_obat() {
        $query = $this->db->get('jenis_obat');
        return $query->result();
    }

    public function get_jenis_obat_by_id($id) {
        $query = $this->db->get_where('jenis_obat', array('id' => $id));
        return $query->row();
    }

    public function insert_jenis_obat($data) {
        return $this->db->insert('jenis_obat', $data);
    }

    public function update_jenis_obat($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('jenis_obat', $data);
    }

    public function delete_jenis_obat($id) {
        $this->db->where('id', $id);
        return $this->db->delete('jenis_obat');
    }
}