<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_users()
    {
        $this->db->select('users.*, roles.role_name, puskesmas.nama_puskesmas');
        $this->db->from('users');
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('puskesmas', 'users.puskesmas_id = puskesmas.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_user_by_id($id)
    {
        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row();
    }

    // Metode untuk mendapatkan user berdasarkan email
    public function get_user_by_email($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('users'); // Ganti 'users' dengan nama tabel user Anda jika berbeda
        return $query->row(); // Mengembalikan satu baris sebagai objek
    }

    public function insert_user($data)
    {
        return $this->db->insert('users', $data);
    }

    public function update_user($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('users', $data);
    }

    public function delete_user($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('users');
    }
}
