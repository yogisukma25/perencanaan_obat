<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail_perencanaan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_detail_perencanaan($id)
    {
        $this->db->select('detail_perencanaan.*, obat.kode_obat as obat_kode, obat.nama_obat as obat_nama, obat.harga as harga, detail_perencanaan.id as detail_id');
        $this->db->from('detail_perencanaan');
        $this->db->join('obat', 'detail_perencanaan.obat_id = obat.id');
        $this->db->where('detail_perencanaan.perencanaan_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_detail_perencanaan_by_id($id)
    {
        $this->db->select('detail_perencanaan.*, obat.nama_obat as obat_nama, obat.harga as harga, detail_perencanaan.id as detail_id');
        $this->db->from('detail_perencanaan');
        $this->db->join('obat', 'detail_perencanaan.obat_id = obat.id');
        $this->db->where('detail_perencanaan.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function insert_detail_perencanaan($data)
    {
        return $this->db->insert('detail_perencanaan', $data);
    }

    public function update_detail_perencanaan($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('detail_perencanaan', $data);
    }

    public function delete_detail_perencanaan($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('detail_perencanaan');
    }
}