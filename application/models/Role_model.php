<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function get_all_roles() {
        $query = $this->db->get('roles');
        return $query->result();
    }

    public function get_role_by_id($id) {
        $query = $this->db->get_where('roles', array('id' => $id));
        return $query->row();
    }

    public function insert_role($data) {
        return $this->db->insert('roles', $data);
    }

    public function update_role($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('roles', $data);
    }

    public function delete_role($id) {
        $this->db->where('id', $id);
        return $this->db->delete('roles');
    }
}